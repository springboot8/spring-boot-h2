# Spring-boot-h2

Exemplo de API Springboot com banco de dados H2. 

## Tecnologias utilizadas

* Java, versão: 8
* Maven
  * Spring Boot, versão: 2.1.13.RELEASE
  * Spring Data Jpa
  * H2 Database

## Urls

Todas as urls responderão no formato **JSON**.

## Estudante
#### Cadastra estudante

```
POST - http://localhost:8091/estudante/cadastro
```

Request
```
{
	"nome": "string",
	"idade": int,
	"email": "string"
}
```

Response 
```
O retorno será o id do estudante cadastrado.
```

#### Recupera lista de estudantes

```
GET - http://localhost:8091/estudante/lista
```

Request
```
Nenhum parâmetro passado na requisição.
```

Response 
```
[
    {
        "id": int,
        "nome": "string",
        "idade": int,
        "email": "string"
    }
]
```

## BANCO DE DADOS H2 - BROWSER URL CONSOLE: 
```
http://localhost:8091/h2-console
```
