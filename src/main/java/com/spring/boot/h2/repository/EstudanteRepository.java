package com.spring.boot.h2.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.spring.boot.h2.entidades.Estudante;

@Repository
public interface EstudanteRepository extends CrudRepository<Estudante, Long> {

}
