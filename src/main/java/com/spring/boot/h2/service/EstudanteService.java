package com.spring.boot.h2.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spring.boot.h2.entidades.Estudante;
import com.spring.boot.h2.repository.EstudanteRepository;

@Service
public class EstudanteService {

	@Autowired
	private EstudanteRepository repository;
	
	public void save(Estudante estudante) {
		repository.save(estudante);
	}
	
	public List<Estudante> lista() {
		List<Estudante> lista = new ArrayList<>();
		repository.findAll().forEach( estudante -> lista.add(estudante));
		return lista;
	}
}
