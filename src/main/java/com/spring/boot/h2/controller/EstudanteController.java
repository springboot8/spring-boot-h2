package com.spring.boot.h2.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.spring.boot.h2.entidades.Estudante;
import com.spring.boot.h2.service.EstudanteService;

@RestController
@RequestMapping("/estudante")
public class EstudanteController {

	@Autowired 
	private EstudanteService service;
	
	@PostMapping("/cadastro")
	public ResponseEntity<?> cadastrar(@RequestBody Estudante estudante) {
		service.save(estudante);
		return ResponseEntity.ok(estudante.getId());
	}
	
	@GetMapping("/lista")
	public ResponseEntity<?> lista() {
		return ResponseEntity.ok(service.lista());
	}
	
}
